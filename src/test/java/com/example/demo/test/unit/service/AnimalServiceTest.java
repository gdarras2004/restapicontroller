package com.example.demo.test.unit.service;

import com.example.demo.dto.AnimalDTO;
import com.example.demo.dto.UpdateAnimalRequestDTO;
import com.example.demo.entity.Animal;
import com.example.demo.mapper.AnimalMapper;
import com.example.demo.repository.AnimalRepository;
import com.example.demo.repository.AnimalTypeRepository;
import com.example.demo.service.AnimalService;
import com.example.demo.test.factory.AnimalFactory;
import com.example.demo.test.factory.AnimalTypeFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@Slf4j
public class AnimalServiceTest {

    @InjectMocks
    private AnimalService animalService;

    @Mock
    private AnimalRepository animalRepository;

    @Mock
    private AnimalTypeRepository animalTypeRepository;

    @Mock
    private AnimalMapper animalMapper;

    private final AnimalFactory animalFactory = new AnimalFactory();
    private final AnimalTypeFactory animalTypeFactory = new AnimalTypeFactory();

    @Before
    public void before() {
        when(animalMapper.toDto(any(Animal.class))).thenReturn(new AnimalDTO());
        animalService = new AnimalService(animalRepository, animalMapper, animalTypeRepository);
    }

    @BeforeEach
    public void beforeEach() {
        log.debug("A test is about to run.");
    }

    @Test
    public void whenGetOne_thenReturnAnimalDTO() {
        when(animalRepository.getOne(anyLong())).thenReturn(animalFactory.getAnimal(1L));

        AnimalDTO animalDTO = animalService.getOne(1L);

        assertThat(animalDTO, instanceOf(AnimalDTO.class));
        verify(animalRepository, times(1)).getOne(anyLong());
    }

    @Test(expected = EntityNotFoundException.class)
    public void whenGetOneNotExist_thenThrowEntityNotFound() {
        when(animalRepository.getOne(anyLong())).thenThrow(EntityNotFoundException.class);

        animalService.getOne(1L);
    }

    @Test
    public void whenUpdate_thenReturnAnimalDTO() {
        doReturn(animalFactory.getAnimal(1L)).when(animalRepository).getOne(anyLong());
        doReturn(animalFactory.getAnimal(1L)).when(animalRepository).save(any(Animal.class));

        assertThat(animalService.update(1L, new UpdateAnimalRequestDTO()), instanceOf(AnimalDTO.class));
        verify(animalRepository, times(1)).getOne(anyLong());
        verify(animalRepository, times(1)).save(any(Animal.class));
        verify(animalTypeRepository, never()).getOne(anyLong());
    }

    @Test
    public void whenUpdate_thenReturnAnimalDTO_getsAnimalType() {
        doReturn(animalFactory.getAnimal(1L)).when(animalRepository).getOne(anyLong());
        doReturn(animalFactory.getAnimal(1L)).when(animalRepository).save(any(Animal.class));
        doReturn(animalTypeFactory.getAnimalType(1L)).when(animalTypeRepository).getOne(anyLong());

        UpdateAnimalRequestDTO updateAnimalRequestDTO = new UpdateAnimalRequestDTO();
        updateAnimalRequestDTO.setTypeId(1L);

        assertThat(animalService.update(1L, updateAnimalRequestDTO), instanceOf(AnimalDTO.class));

        verify(animalRepository, times(1)).getOne(anyLong());
        verify(animalRepository, times(1)).save(any(Animal.class));
        verify(animalTypeRepository, times(1)).getOne(anyLong());
    }

    @Test
    public void whenFindAll_thenReturnList() {
        List<Animal> arrayList = new ArrayList<>();
        arrayList.add(new Animal());
        arrayList.add(new Animal());
        doReturn(arrayList).when(animalRepository).findAll();
        doReturn(new AnimalDTO()).when(animalMapper).toDto(any(Animal.class));

        animalService.findAll();

        verify(animalRepository, times(1)).findAll();
        verify(animalMapper, times(2)).toDto(any());
    }

    @Test
    public void whenDeleteExisting_thenDoNothing() {
        doNothing().when(animalRepository).deleteById(anyLong());

        animalService.delete(anyLong());

        verify(animalRepository, times(1)).deleteById(anyLong());
    }

    @Test(expected = EntityNotFoundException.class)
    public void whenDeleteNonExisting_thenThrowException() {
        doThrow(EntityNotFoundException.class).when(animalRepository).deleteById(anyLong());

        animalService.delete(anyLong());
    }

    @Test(expected = EntityNotFoundException.class)
    public void whenTestMethod_thenThrowEntityNotFound() {
        animalService.testMethod();
    }
}