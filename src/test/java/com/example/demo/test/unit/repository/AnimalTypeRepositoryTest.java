package com.example.demo.test.unit.repository;

import com.example.demo.entity.AnimalType;
import com.example.demo.repository.AnimalTypeRepository;
import com.example.demo.test.config.H2JpaConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = H2JpaConfig.class)
@ActiveProfiles({"test"})
public class AnimalTypeRepositoryTest {

    @Autowired
    AnimalTypeRepository animalTypeRepository;

    @Test
    public void whenFindTypeByName_returnList() {
        List<String> shouldReturn = new ArrayList<>();
        shouldReturn.add("Animal_Type_1");
        shouldReturn.add("Animal_Type_2");

        List<AnimalType> found = animalTypeRepository.findTypeByName("Same_Name");

        assertArrayEquals(shouldReturn.toArray(), found.stream().map(AnimalType::getValue).toArray());
        assertEquals("Animal_Type_1", found.get(0).getValue());
        assertEquals(2, found.size());
    }

}