package com.example.demo.test.unit.controller;

import com.example.demo.controller.AnimalController;
import com.example.demo.dto.AnimalDTO;
import com.example.demo.mapper.AnimalMapper;
import com.example.demo.mapper.AnimalTypeMapper;
import com.example.demo.service.AnimalService;
import com.example.demo.test.factory.AnimalFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AnimalController.class)
@ActiveProfiles("test")
public class AnimalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AnimalService animalService;

    @Before
    public void before() {
        AnimalMapper animalMapper = new AnimalMapper(new AnimalTypeMapper());
        List<AnimalDTO> animalDTOList = new AnimalFactory().getAnimalList().stream().map(animalMapper::toDto).collect(Collectors.toList());
        given(animalService.findAll()).willReturn(animalDTOList);
        animalDTOList.forEach(animalDTO -> given(animalService.getOne(animalDTO.getId())).willReturn(animalDTO));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void whenGetAll_returnList() throws Exception {
        mockMvc.perform(get("/animal"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void whenGetOne_returnAnimalDTO() throws Exception {
        mockMvc.perform(get("/animal/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is("Name_1")))
                .andExpect(jsonPath("type.value", is("Animal_Type_1")));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void deleteAsUser_isForbidden () throws Exception {
        mockMvc.perform(delete("/animal/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void deleteAsAdmin_isOK () throws Exception {
        mockMvc.perform(delete("/animal/1"))
                .andExpect(status().isOk());
    }

}
