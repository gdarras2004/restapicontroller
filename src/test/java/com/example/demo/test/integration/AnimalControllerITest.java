package com.example.demo.test.integration;

import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = DemoApplication.class)
@AutoConfigureMockMvc
@TestPropertySource( locations = "classpath:application-test.yml")
@ActiveProfiles("test")
public class AnimalControllerITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(roles = {"USER"})
    public void whenGetAll_returnList() throws Exception {
        mockMvc.perform(get("/animal"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void whenGetOne_returnAnimalDTO() throws Exception {
        mockMvc.perform(get("/animal/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is("Name_2")))
                .andExpect(jsonPath("type.value", is("Animal_Type_1")));
    }

    @Test
    @WithMockUser(roles = {"USER"})
    public void whenGetOne_isNotOK() throws Exception {
        mockMvc.perform(get("/animal/123123123"))
                .andExpect(status().isNotFound());
    }

}
