package com.example.demo.test.factory;

import com.example.demo.entity.Animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class AnimalFactory {
    private final List<String> colors = new ArrayList<String>(Arrays.asList("green", "red", "yellow", "brown"));

    AnimalTypeFactory animalTypeFactory = new AnimalTypeFactory();

    private final List<Animal> animalList = new ArrayList<>();

    public AnimalFactory() {
        for (int i = 0; i < 10; i++) {
            animalList.add(getAnimal((long) i));
        }
    }

    public List<Animal> getAnimalList() {
        return animalList;
    }

    public Animal getAnimal(Long id) {
        Random random = new Random();
        Animal animal = new Animal();
        animal.setId(id);
        animal.setFeet((long) (random.nextBoolean() ? 2 : 4));
        animal.setColor(colors.get(random.nextInt(colors.size())));
        animal.setType(animalTypeFactory.getAnimalType(id));
        animal.setAge(random.nextInt(30));
        animal.setName("Name_" + id);
        return animal;
    }
}
