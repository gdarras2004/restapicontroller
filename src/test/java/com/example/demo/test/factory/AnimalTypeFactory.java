package com.example.demo.test.factory;

import com.example.demo.entity.AnimalType;

import java.util.ArrayList;
import java.util.List;

public class AnimalTypeFactory {
    private final List<AnimalType> animalTypeList = new ArrayList<>();

    public AnimalTypeFactory() {
        for (int i = 0; i < 3; i++) {
            animalTypeList.add(getAnimalType((long) i));
        }
    }

    public List<AnimalType> getAnimalTypeList() {
        return animalTypeList;
    }

    public AnimalType getAnimalType(Long id) {
        AnimalType animalType = new AnimalType();
        animalType.setValue("Animal_Type_" + id);
        animalType.setId(id);
        return animalType;
    }
}
