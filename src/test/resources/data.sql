INSERT INTO animal_type (id, value) VALUES (1, 'Animal_Type_1'),
                                           (2, 'Animal_Type_2'),
                                           (3, 'Animal_Type_3'),
                                           (4, 'Animal_Type_4'),
                                           (5, 'Animal_Type_5');

INSERT INTO animal (id, age, animal_name, type_id, feet, color) VALUES (1, 1, 'Same_Name', 1, 4, 'black'),
                                                                       (2, 1, 'Name_2', 1, 4, 'blue'),
                                                                       (3, 1, 'Same_Name', 2, 4, 'white'),
                                                                       (4, 1, 'Name_4', 4, 4, 'red'),
                                                                       (5, 1, 'Name_5', 5, 4, 'brown');
