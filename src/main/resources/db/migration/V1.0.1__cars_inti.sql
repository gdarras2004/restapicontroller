insert into car
values (1100, 'BMW', 200),
       (1101, 'Opel', 123),
       (1102, 'Volkswagen', 32),
       (1103, 'Volvo', 54),
       (1104, 'Suzuki', 65),
       (1200, '#BMW', 200),
       (1201, '#Opel', 123),
       (1202, '#Volkswagen', 32),
       (1203, '#Volvo', 54),
       (1204, '#Suzuki', 65);