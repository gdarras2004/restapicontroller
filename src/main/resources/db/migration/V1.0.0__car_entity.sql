create table car
(
    id bigint auto_increment
        primary key,
    brand varchar(255) null,
    power int not null
);
