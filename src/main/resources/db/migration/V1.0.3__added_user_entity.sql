create table user
(
    id bigint auto_increment primary key,
    username varchar(255) unique not null,
    password varchar (255) not null,
    authorities text not null,
    active boolean default true
);
