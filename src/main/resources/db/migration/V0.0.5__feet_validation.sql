update animal a
set feet = 0
where feet is null;

ALTER TABLE animal MODIFY feet INT NOT NULL DEFAULT 0;