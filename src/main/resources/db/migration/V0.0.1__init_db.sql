create table animal_type
(
    id bigint auto_increment
        primary key,
    value varchar(255) null
);

create table animal
(
    id bigint auto_increment
        primary key,
    age int not null,
    animal_name varchar(255) null,
    type_id bigint not null,
    constraint FK8kj84igd1a5os9teafkf2qsyp
        foreign key (type_id) references animal_type (id)
);
