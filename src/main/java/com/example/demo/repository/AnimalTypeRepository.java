package com.example.demo.repository;

import com.example.demo.entity.AnimalType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalTypeRepository extends JpaRepository<AnimalType, Long> {

    @Query("SELECT t FROM AnimalType t LEFT JOIN Animal a ON a.type = t WHERE a.name = :name")
    List<AnimalType> findTypeByName(@Param("name") String name);
}
