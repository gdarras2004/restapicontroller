package com.example.demo.mapper;

import com.example.demo.dto.AnimalDTO;
import com.example.demo.entity.Animal;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AnimalMapper {
    private final AnimalTypeMapper animalTypeMapper;

    public AnimalDTO toDto(Animal animal) {
        return AnimalDTO.builder().id(animal.getId())
                .name(animal.getName())
                .age(animal.getAge())
                .type(animalTypeMapper.toDto(animal.getType()))
                .color(animal.getColor())
                .feet(animal.getFeet())
                .build();
    }
}
