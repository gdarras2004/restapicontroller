package com.example.demo.mapper;

import com.example.demo.dto.AnimalTypeDTO;
import com.example.demo.entity.AnimalType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AnimalTypeMapper {

    public AnimalTypeDTO toDto(AnimalType animalType) {
        return AnimalTypeDTO.builder()
                .id(animalType.getId())
                .value(animalType.getValue())
                .build();
    }
}
