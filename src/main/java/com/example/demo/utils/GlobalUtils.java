package com.example.demo.utils;

import java.util.Calendar;

public class GlobalUtils {

    public static Long getTimeMillis() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
