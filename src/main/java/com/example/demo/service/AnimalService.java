package com.example.demo.service;

import com.example.demo.dto.AnimalDTO;
import com.example.demo.dto.CreateAnimalRequestDTO;
import com.example.demo.dto.UpdateAnimalRequestDTO;
import com.example.demo.entity.Animal;
import com.example.demo.mapper.AnimalMapper;
import com.example.demo.repository.AnimalRepository;
import com.example.demo.repository.AnimalTypeRepository;
import com.example.demo.utils.GlobalUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class AnimalService {

    private final AnimalRepository animalRepository;
    private final AnimalMapper animalMapper;
    private final AnimalTypeRepository animalTypeRepository;

    public List<AnimalDTO> get3() {
//        List<AnimalDTO> list = new ArrayList<>();
//        for (Animal animal : animalRepository.findAll()) {
//            AnimalDTO animalDTO = animalMapper.toDto(animal);
//            list.add(animalDTO);
//        }
//        return list;
//
        return animalRepository.findTop3ByOrderByIdAsc().stream().map(animalMapper::toDto).collect(Collectors.toList());
    }

    public List<AnimalDTO> findAll() {
//        List<AnimalDTO> list = new ArrayList<>();
//        for (Animal animal : animalRepository.findAll()) {
//            AnimalDTO animalDTO = animalMapper.toDto(animal);
//            list.add(animalDTO);
//        }
//        return list;
//
        return animalRepository.findAll().stream().map(animalMapper::toDto).collect(Collectors.toList());
    }

    public AnimalDTO create(CreateAnimalRequestDTO createRequest) {
        System.out.println(GlobalUtils.getTimeMillis());
        Animal newAnimal = new Animal();
        newAnimal.setName(createRequest.getName());
        newAnimal.setAge(createRequest.getAge());
        newAnimal.setType(animalTypeRepository.getOne(createRequest.getTypeId()));
        newAnimal.setColor(createRequest.getColor());
        newAnimal.setFeet(createRequest.getFeet());
        newAnimal = animalRepository.save(newAnimal);
        return animalMapper.toDto(newAnimal);
    }

    public void delete(Long id) {
        animalRepository.deleteById(id);
    }

    public AnimalDTO update(Long id, UpdateAnimalRequestDTO updateRequest) {
        Animal animal = animalRepository.getOne(id);
        if (Strings.isNotBlank(updateRequest.getName())) {
            animal.setName(updateRequest.getName());
        }
        if (updateRequest.getAge() != null) {
            animal.setAge(updateRequest.getAge());
        }
        if (updateRequest.getTypeId() != null) {
            animal.setType(animalTypeRepository.getOne(updateRequest.getTypeId()));
        }
        if (Strings.isNotBlank(updateRequest.getColor())) {
            animal.setColor(updateRequest.getColor());
        }
        if (updateRequest.getFeet() != null) {
            animal.setFeet(updateRequest.getFeet());
        }
        animal = animalRepository.save(animal);
        return animalMapper.toDto(animal);
    }

    public AnimalDTO getOne(Long id) {
        return animalMapper.toDto(animalRepository.getOne(id));
    }

    public void testMethod() {
        throw new EntityNotFoundException("[test] Entity not found.");
    }
}
