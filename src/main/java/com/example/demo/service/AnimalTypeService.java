package com.example.demo.service;

import com.example.demo.dto.AnimalTypeDTO;
import com.example.demo.dto.CreateAnimalTypeRequestDTO;
import com.example.demo.dto.UpdateAnimalTypeRequestDTO;
import com.example.demo.entity.AnimalType;
import com.example.demo.mapper.AnimalTypeMapper;
import com.example.demo.repository.AnimalTypeRepository;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AnimalTypeService {

    private final AnimalTypeRepository animalTypeRepository;
    private final AnimalTypeMapper animalTypeMapper;

    public List<AnimalTypeDTO> findAll() {
        return animalTypeRepository.findAll().stream().map(animalTypeMapper::toDto).collect(Collectors.toList());
    }

    public AnimalTypeDTO create(CreateAnimalTypeRequestDTO createRequest) {
        AnimalType newAnimalType = new AnimalType();
        newAnimalType.setValue(createRequest.getValue());
        newAnimalType = animalTypeRepository.save(newAnimalType);
        return animalTypeMapper.toDto(newAnimalType);
    }

    public void delete(Long id) {
        animalTypeRepository.deleteById(id);
    }

    public AnimalTypeDTO update(Long id, UpdateAnimalTypeRequestDTO updateRequest) {
        AnimalType animalType = animalTypeRepository.getOne(id);
        if (Strings.isNotBlank(updateRequest.getValue())) {
            animalType.setValue(updateRequest.getValue());
        }
        animalType = animalTypeRepository.save(animalType);
        return animalTypeMapper.toDto(animalType);
    }

    public AnimalTypeDTO getOne(Long id) {
        return animalTypeMapper.toDto(animalTypeRepository.getOne(id));
    }

    public List<AnimalTypeDTO> getByAnimalName(String animalName) {
        return animalTypeRepository.findTypeByName(animalName).stream().map(animalTypeMapper::toDto).collect(Collectors.toList());
    }

}
