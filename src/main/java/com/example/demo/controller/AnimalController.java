package com.example.demo.controller;

import com.example.demo.dto.AnimalDTO;
import com.example.demo.dto.CreateAnimalRequestDTO;
import com.example.demo.dto.UpdateAnimalRequestDTO;
import com.example.demo.service.AnimalService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("animal")
@AllArgsConstructor
@Slf4j
public class AnimalController {

    private final AnimalService animalService;

    @GetMapping
    public List<AnimalDTO> getAll(Principal principal) {
        log.info("[animalController] getAll ({})", principal);
        return animalService.findAll();
    }

    @GetMapping("/get3")
    public List<AnimalDTO> get3() {
        return animalService.get3();
    }

    @GetMapping(path = "{id}")
    public AnimalDTO getOne(@PathVariable("id") Long id) {
        return animalService.getOne(id);
    }

    @PostMapping
    public AnimalDTO create(@RequestBody @Validated CreateAnimalRequestDTO createRequest) {
        return animalService.create(createRequest);
    }

    @PutMapping(path = "{id}")
    public AnimalDTO update(@RequestBody UpdateAnimalRequestDTO updateRequest, @PathVariable("id") Long id) {
        return animalService.update(id, updateRequest);
    }

    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable("id") Long id) {
        animalService.delete(id);
    }
}
