package com.example.demo.controller;

import com.example.demo.dto.AnimalTypeDTO;
import com.example.demo.dto.CreateAnimalTypeRequestDTO;
import com.example.demo.dto.UpdateAnimalTypeRequestDTO;
import com.example.demo.service.AnimalTypeService;
import com.example.demo.utils.GlobalUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("animalType")
@AllArgsConstructor
@Slf4j
public class AnimalTypeController {

    private final AnimalTypeService animalTypeService;

    @GetMapping
    public List<AnimalTypeDTO> getAll(Principal principal) {
        log.info("[animalTypeController] {} {}", GlobalUtils.getTimeMillis(), "This is it.");
        log.info("[animalTypeController] getAll ({})", principal);
        return animalTypeService.findAll();
    }

    @GetMapping(path = "{id}")
    public AnimalTypeDTO getOne(@PathVariable("id") Long id) {
        return animalTypeService.getOne(id);
    }

    @PostMapping
    public AnimalTypeDTO create(@RequestBody CreateAnimalTypeRequestDTO createRequest) {
        return animalTypeService.create(createRequest);
    }

    @PutMapping(path = "{id}")
    public AnimalTypeDTO update(@RequestBody UpdateAnimalTypeRequestDTO updateRequest, @PathVariable("id") Long id) {
        return animalTypeService.update(id, updateRequest);
    }

    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable("id") Long id, Principal principal) {
        log.info("[animalTypeController] delete {}", principal);
        animalTypeService.delete(id);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(path = "/animalName/{name}")
    public List<AnimalTypeDTO> byAnimalName(@PathVariable("name") String animalName) {
        return animalTypeService.getByAnimalName(animalName);
    }
}
