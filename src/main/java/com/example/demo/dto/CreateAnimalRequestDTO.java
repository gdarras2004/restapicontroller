package com.example.demo.dto;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateAnimalRequestDTO {

    private String name;
    private Integer age;
    private Long typeId;

    @NotNull
    @Max(4)
    @Min(2)
    private Long feet;
    private String color;

}
