package com.example.demo.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnimalDTO {

    private Long id;
    private String name;
    private Integer age;
    private Long feet;
    private String color;
    private AnimalTypeDTO type;

}
