package com.example.demo.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateAnimalRequestDTO {

    private String name;
    private Integer age;
    private Long typeId;
    private String color;
    private Long feet;

}
